Input parameters for DIETER
===========================

This documentation page aims at providing insights on up to date calibration data 
used in the DIETER power system model. **DIETER** is a model developed at the DIW Berlin (German Institute of Economic Research).
It aims at investigating power sector developments that would enable to secure a fully electrified energy system
relying on renewable energy sources. 
For more information, please visit the `DIW website <https://www.diw.de/de/diw_01.c.599753.de/modelle.html/#ab_599749/>`_ 
or refer to the :ref:`DIETER section<DIETER section>` of this documentation webpage.

.. important::

   This project is under active development.

