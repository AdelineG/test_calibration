.. |br| raw:: html

    <br>

*****
Team 
*****

The team of DIETER core developers is a group of Post-Docs and Ph.D. students at DIW Berlin lead by Dr. Wolf-Peter Schill.
We are part of the Energy, Environment and Transportation department (in German Energie, Umwelt, Verkehr (EVU)) and form, 
within this department, the Transformation of the Energy Economy (TEE) working group. You can follow the main activities of
our group on Twitter.

Current developers
******************

*Wolf-Peter Schill*
""""""""""""""""""""

.. image:: https://www.diw.de/sixcms/media.php/37/thumbnails/WSchill.jpg.568394.jpg
      :width: 150

Wolf is Deputy Head of the Department Energy, Transportation, Environment at DIW Berlin and leads the group Transformation of the Energy Economy. He is interested in various aspects of the renewable energy transformation. Together with Alexander Zerrahn, he is one of the fathers of the DIETER model. He made substantial contributions to the initial model development, different types of model extensions, and various applications.

**DIETER Expertise:** |br|
+ A bit of everything

**E-Mail:** wschill@diw.de |br| **Work:** `Google Scholar <https://scholar.google.com/citations?user=Y6aa6xgAAAAJ&hl=de&oi=sra>`_

|

*Carlos Gaete-Morales*
"""""""""""""""""""""""

.. image:: https://www.diw.de/sixcms/media.php/37/thumbnails/CGaete.jpg.574923.jpg
  :width: 150

Carlos is a research associate at DIW Berlin, holds a PhD from The University of Manchester and an Industrial Engineering degree. His research focuses on the sustainability assessment of energy systems and finding sustainable pathways for future energy economy by implementing optimization models, life cycle assessment (LCA), and machine learning techniques. He is interested in outreach his work by developing open-source tools using Python and is the father of the Python wrapper for DIETER, that is DIETERpy.

**DIETER Expertise:** |br|
+ Model analyses of settings with high renewables |br|
+ e-Mobility, see also `emobpy <https://pypi.org/project/emobpy>`_ |br|
+ Energy storage |br|

**E-Mail:** cgaete@diw.de |br| **Work:** `Google Scholar <https://scholar.google.com/citations?user=Cay15a0AAAAJ&hl=en&oi=ao>`_

|

*Alexander Roth*
""""""""""""""""""

.. image:: https://www.diw.de/sixcms/media.php/37/thumbnails/ARoth.jpg.551959.jpg
  :width: 150

Alex is a research associate and PhD candidate at DIW Berlin. He works currently on a multi-country version of DIETERpy and the interaction effects of electricity storages.

**DIETER Expertise:** |br|
+ Multi-country applications |br|
+ Electricity storage

**E-Mail:** aroth@diw.de |br| **Work** `Google Scholar <https://scholar.google.de/citations?hl=de&user=WF4xfL4AAAAJ>`_

|

*Martin Kittel*
""""""""""""""""""

.. image:: https://www.diw.de/sixcms/media.php/37/thumbnails/MKittel.jpg.551922.jpg
  :width: 150

Martin is a research associate and PhD candidate at DIW Berlin. He mainly develops and maintains our stylized DIETERpy model, and renewable energy constraints in DIETERpy. In his current work, Martin investigates the impact of renewable energy constraints on power sector and energy system models.

**Expertise:** |br|
+ Renewable energy constraints |br|
+ Power-to-power storage |br|
+ Cluster analysis

**E-Mail:** mkittel@diw.de |br| **Work:** `Google Scholar <https://scholar.google.com/citations?user=wpZdqusAAAAJ&hl=de&oi=sra>`_

|

*Dana Kirchem*
""""""""""""""

*Adeline Guéret*
""""""""""""""""