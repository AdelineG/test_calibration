******************
External resources 
******************

Open-source modelling of power systems
**************************************

Different communities, such as `OpenMod <https://openmod-initiative.org/>`_, already exist to curate the most up-to-date parameters for power system models. 
The aim of our documentation webpage is not to compete with already existing communities and platforms providing open access to up-to-date data. 
The aim is rather to pursue efforts to open the black box of modelling as much as possible. In our case, it means providing open access to and keep track of sources that were used 
for the development of DIETER.