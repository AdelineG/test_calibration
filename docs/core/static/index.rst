.. _Core model section:

Static data
************

Static data refer to techno-economic parameters that are considered fixed over time. 
This means they are taken as granted by the model over the whole optimization period of 8760 hours. 
In this section, we describe the static data that are needed in the core model that is to say that are used for the baseline scenario to run.
More precisely, we distinguish between **spatial data** and **technology data** and further split technology data into two categories:
*costs data* and *potentials data*.

* The *spatial data* considered are:
   * Incidence matrix
   * Link between countries 
   * Maximum Net Transfer Capacity (NTC)
   * Distance between geographic centers 
   * Transmission losses
   * Overnight investment costs (for transmission lines)
   * Annual fixed costs (for transmission lines)
   * Variable operation and maintenance (OM) cost (for transmission lines)
   * Recovery period (for transmission lines)
   * Technical lifetime (for transmission lines)
   * Interest rate (for transmission lines)
   * Historical NTCs 

* The *costs data* considered are: 
   * Annuel fixed cost per MW
   * Operation and maintenance (OM) cost per MWh
   * Overnight investment costs per MW
   * Interest rate for calculating investment annuities
   * Load change costs for changing generation upward
   * Load change costs for changing generation downward
   * Fuel costs
   * CO2 price 
   * Curtailment costs

* The *potentials data* considered are: 
   * Thermal efficiency 
   * Carbon content of fossil fuels 
   * Time-constant availability
   * Technical lifetime 
   * Maximum possible investment 
   * Maximum yearly energy 
   * Flexibility of changing load level
   * Fixed exogenous capacities for model application


.. toctree::
   :hidden:
   
   Spatial data <spatial/index.rst>
   Costs data <costs/index.rst>
   Potentials data <potentials/index.rst>