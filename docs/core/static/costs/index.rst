Costs
+++++

.. csv-table:: Overview of costs in DIETER (core model)  
   :file: costs.csv
   :widths: 30,20,20,30
   :header-rows: 1

.. toctree::
    :hidden:

    Conventional <conventional.rst>
    Solar <solar.rst>
    Wind <wind.rst>