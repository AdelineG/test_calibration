Potentials
++++++++++

.. csv-table:: Overview of potentials data in DIETER (core model)
    :file: potentials.csv
    :widths: 30,20,20,30
    :header-rows: 1

.. toctree::
    :hidden:

    Conventional <conventional.rst>
    Solar <solar.rst>
    Wind <wind.rst>