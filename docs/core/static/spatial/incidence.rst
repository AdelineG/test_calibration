Incidence matrix
----------------

The incidence matrix allows to define the geographic scope of the model and 
the links between the various countries that are taken into account. Usually, it shows in rows as well as in columns 
every country considered in the model. When two countries are connected by a transmission line, 
the corresponding entry in the matrix is filled with 1. Otherwise, it is filled with 0. Since transmission lines are polarized, the convention
is to read as the destination country the one that is displayed in the column whereas the row country is considered as the starting point of the line. 

In DIETER, the incidence matrix is defined a bit differently. It takes as column the countries that are considered in the model and 
as rows existing connections between countries. For instance, we know that Austria (AT) is connected with Switzerland (CH), Czech Republic (CZ) and Italy (IT).
Hence, in rows we display AT_CH, AT_CZ and AT_IT. There should be as many rows in the matrix as connections between countries. For each row, 
we then fill with 1 the column entry that corresponds to the first country and with -1 the column entry that corresponds to the second country of the link. 
One important rule is that the sum over the row, for each row, should amount to 0. 

The current incidence matrix design is shown in the table below.

.. csv-table:: Incidence matrix in DIETER
    :file: incidence.csv
    :widths: 12, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8
    :header-rows: 1
