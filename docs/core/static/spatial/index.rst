Spatial data in DIETER
++++++++++++++++++++++

.. csv-table:: Overview of spatial data in DIETER (core model)  
   :file: spatial.csv
   :widths: 30,20,20,30
   :header-rows: 1

.. toctree::
    :hidden: 

    Incidence matrix <incidence.rst>