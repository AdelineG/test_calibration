Time series
************

.. toctree::
   :hidden:
   
   Demand <demand/index.rst>  

.. toctree::
   :hidden:

   Capacity factors <availability/index.rst>