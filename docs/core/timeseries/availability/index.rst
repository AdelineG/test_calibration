Capacity factors
++++++++++++++++

.. toctree::
    :hidden:

    Solar <solar.rst>
    Wind onshore <windon.rst>
    Wind offshore <windoff.rst>