Demand-side management
**********************

.. toctree::
    :hidden:

    Static data <static/index.rst>
    Time series <timeseries/index.rst>