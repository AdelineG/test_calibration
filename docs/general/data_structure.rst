
*******************************
Input data structure of DIETER
*******************************

Different types of data are needed and fed into the model exogeneously. In the current implementation, we first distinguish between 
static data and time series. 

* **Static data** refer to techno-economic parameters that are considered fixed over time. 
  This means they are taken as granted by the model over the whole optimization period of 8760 hours. 
  Among static data, we made a distinction between data that are needed in the core model, that is to say that are used for 
  the baseline scenario to run, and data that comes along additional features that one can chose to complexify the representation 
  of a given feature in the model. 

* **Time series** refer to data parameters that can vary over time during the year. It means that parameters have to be specified 
  for each and every hour of the year.

This distinction between static data and time series structure the whole input data of DIETER. For clarity purposes, we further distinguish between 
data that are used in the core model and data that are used for additional modules of the model that can be activated or deactivated separately. 
Hence, we find static data and time series data detailed module by module. Nevertheless, in the concrete implementation of the model, a slightly
different organization is used, namely one file for static data (core and modules) and one file for time series (core and modules). We use
different spreadsheets inside a given file in order to distinguish between the core model and the different modules. 

Core model data
****************

* **Spatial data**: defines the spatial structure of the model i.e. the geographical scope (how many nodes are represented in the model) 
  and the connections between nodes. In particular, it defines the existing geographic connection between border-sharing countries 
  e.g. Germany is connected to France, Denmark, Belgium, the Netherlands, Czech Republic, Poland, Austria and Switzerland. 
  Each connection between two nodes is called a “link” and is given a name e.g. “l1” for the connection between Germany and France for 
  instance. For each link, some characteristics are given: maximum installable capacities, distance, overnight costs, fixed costs, 
  recovery period, lifetime, interest rate and fixed NTC. 

* **Technology data**: overall, there are 11 technologies considered. Each technology is classified as conventional (conv) or renewable (res) as well as dispatchable (dis)
  or non-dispatchable (nondis). These technologies and the corresponding classification are detailed in the table below. 
  Apart from this classification, there are numerous parameters (18 in total) defined for each generation technology. 
  They are exhaustively documented in the :ref:`core model section<Core model section>` of this documentation page.

.. csv-table:: Technologies in DIETER
   :file: technologies.csv
   :widths: 40,20,20,20
   :header-rows: 1

Additional modules
******************

The core model can be extended to feature a more detailed representation of particular aspects such as the introduction of storages
and reservoirs, demand-side management (DSM), electric vehicles, prosumage, heat and hydrogen. The data structure as well as sources of
different modules are detailed in the section 

