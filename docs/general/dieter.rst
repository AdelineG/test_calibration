.. _DIETER section:

******
DIETER 
******

The Dispatch and Investment Evaluation Tool with Endogenous Renewables (DIETER) has initially been developed to study the role of electricity storage and other flexibility options 
in greenfield scenarios with high shares of variable renewable energy sources. The model minimizes overall system costs in a long-run equilibrium setting, determining least-cost capacity 
expansion and use of various generation and flexibility technologies. DIETER can capture multiple system benefits of electricity storage related to capacity, energy arbitrage and reserve 
provision.

DIETER is an open source model which may be freely used and modified by anyone. The code is licensed under the MIT License. Input data is licensed under the
Creative Commons Attribution-ShareAlike 4.0 International Public License.

The documentation of the model is available `here <https://gitlab.com/diw-evu/dieter_public/dietergms/-/blob/master/DIETER_documentation.pdf>`_. 
For more information on the different available versions of the model, please visit the `DIW website <https://www.diw.de/en/diw_01.c.599753.en/models.html#ab_600546>`_. 

DIETERpy
********

DIETERpy is a Python-based tool that enables an easy pre- and post-processing of the model data, sophisticated scenario analysis, and visualization of results. 
The optimization routine of DIETERpy is based on the General Algebraic Modeling System (GAMS) of DIETER, which is now maintained separately as a GAMS-only version called DIETERgms.

To learn more about this project, please consult the `DIETERpy project description <https://pypi.org/project/dieterpy/>`_,  
navigate through the corresponding `documentation <https://diw-evu.gitlab.io/dieter_public/dieterpy/index.html>`_ 
or visit the `GitLab public repository <https://gitlab.com/diw-evu/dieter_public/dieterpy>`_.

The figure below shows the workflow between Python and GAMS.

.. figure:: dieterpy.png 

   © Carlos Gaete-Morales