*****************
General framework
*****************

Power system models heavily rely on input data that are used to calibrate them. In particular, costs, potentials and availabilities of renewable sources are of cardinal importance.
As technologies evolve and mature, these parameters tend to change. Hence, the necessity to keep the calibration up-to-date. 
When it comes to renewable technologies such as wind, solar and hydro power as well as biomass, technologies have very quickly evolved over the last decade. 
This rapid change makes it all the more necessary to update power system models with accurate and recent data. 
On the other hand, it represents also a challenge as the effect of a high-paced innovation combined with a rapid market diffusion make it difficult to get 
stable estimations for these parameters. 



