.. include:: ../README.rst

.. toctree::
   :caption: General
   :hidden:
   :maxdepth: 1
   
   Framework <general/framework.rst>  
   DIETER <general/dieter.rst>
   Data structure <general/data_structure.rst>

.. toctree::
   :caption: Core model
   :hidden:

   Static data <core/static/index.rst>
   Time series <core/timeseries/index.rst>   

.. toctree::
   :caption: Additional modules
   :hidden:
   
   Storage <modules/storages/index.rst>
   Reservoirs <modules/reservoirs/index.rst>
   DSM <modules/dsm/index.rst>
   Electric vehicles <modules/ev/index.rst>
   Prosumage <modules/prosumage/index.rst>
   Heat <modules/heat/index.rst>
   Hydrogen <modules/hydrogen/index.rst>

.. toctree::
   :caption: Misc
   :hidden:
   
   Team <misc/team.rst>
   Contact <misc/contact.rst>
   External resources <misc/external.rst>
   Glossary <misc/glossary.rst>


